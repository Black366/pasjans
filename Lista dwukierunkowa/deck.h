#pragma once
#include "stdafx.h"
#include <string>
#include <iostream>
using namespace std;

struct card
{
	enum rodzaje{ pik, kier, karo, trefl };
	rodzaje kolor;
	int value;
	card * prev;
	card * next;
	void display();
};

class deck
{
private:

	//card * HEAD, *TAIL;
	unsigned counter;

public:
	card * HEAD, *TAIL;
	//konstruktor
	deck();
	~deck();
	card * push_front(card *p);
	card * push_back(card * p);
	card * pop_front();
	card * pop_back();
	card * insert(card *p, card *behind);  // wstawianie p za behind
	unsigned size();
	card* index(unsigned n); // numerowanie kart od 1 nie od 0
	void display();
	void genDeck();
	card * pick(card * p);
	void shuffle();
};