#include "stdafx.h"
#include "deck.h"
#include <ctime>
#include <iostream>
using namespace std;
void card::display()
	{
		switch (this->value)
		{
	case 14:
		cout << "As ";
		break;
	case 13:
		cout << "Krol ";
		break;
	case 12:
		cout << "Dama ";
		break;
	case 11:
		cout << "Walet ";
		break;
	default:
		cout << this->value << " ";
		break;
	}

		switch (this->kolor)
		{
		case 0:
			cout << "pik" << endl;
			break;
		case 1:
			cout << "kier" << endl;
			break;
		case 2:
			cout << "karo" << endl;
			break;
		case 3:
			cout << "trefl" << endl;
			break;
		}
	}
	//konstruktor
	deck::deck()
	{
		HEAD = NULL;
		TAIL = NULL;
	}
	//destruktor
	deck::~deck()
	{
		card * p;

		while (HEAD)
		{
			p = HEAD->next;
			delete HEAD;
			HEAD = p;
		}
	}

	card * deck::push_front(card *p)
	{
		p->next = HEAD;
		p->prev = NULL;
		if (HEAD) HEAD->prev = p;
		HEAD = p;
		if (!TAIL) TAIL = HEAD;
		counter++;
		return HEAD;
	}

	card * deck::push_back(card * p)
	{
		if (TAIL) TAIL->next = p;
		p->next = NULL; p->prev = TAIL;
		TAIL = p;
		if (!HEAD) HEAD = TAIL;
		counter++;
		return TAIL;
	}

	card* deck::pop_front() //zabiera z poczatku
	{
		card * p, *p1;
		if (!HEAD)
		{
			cout << "Blad, lista jest pusta \n";
			return NULL;
		}
		else
		{
			p = HEAD->next;
			p->prev = NULL;
			HEAD->prev = NULL;
			HEAD->next = NULL;
			p1 = HEAD;
			HEAD = p;
			counter--;
			return p1;
		}
	}

	card* deck::pop_back() //zabiera z konca
	{
		card * p, *p1;
		if (!TAIL)
		{
			cout << "Blad, lista jest pusta \n";
			return NULL;
		}
		else
		{
			p = TAIL->prev;
			p->next = NULL;
			TAIL->prev = NULL;
			TAIL->next = NULL;
			p1 = TAIL;
			TAIL = p;
			counter--;
			return p1;
		}
	}

	card * deck::insert(card *p, card *behind)  // wstawianie p za behind
	{
		/*p->next = behind->next;
		if (behind->next)
			behind->next->prev = p;
		else
			TAIL = p;
		behind->next = p;
		p->prev = behind;
		counter++;
		return p;*/
		if (behind != NULL && p != NULL)
		{
			p->next = behind->next; p->prev = behind;
			behind->next = p;
			if (p->next) p->next->prev = p;
			else TAIL = p;
			counter++;
			return p;
		}
		else
		{
			cout << "\nInsert error!!\n";
			return NULL;
		}
		//if ()
	}

	unsigned deck::size()
	{
		return counter;
	}

	card* deck::index(unsigned n) // numerowanie kart od 1 nie od 0
	{
		card* p;
		//if (n <= 0 || n>counter)
		if ((n<1) || (n > counter))
		{
			cout << "\n Index error! \n";
			return NULL;
		}
		else if (n == counter) return TAIL;
		else if (n < counter / 2) // szukanie w dolnej polowie
		{
			p = HEAD;
			while (--n) p = p->next;
			return p;
		}
		else // szukanie w gornej polowie
		{
			p = TAIL;
			while (counter > n++) p = p->prev;
			return p;
		}
	}

	card * deck::pick(card * p)
	{
		if (p != NULL)
		{
			//card * p1;
			if (p->prev) p->prev->next = p->next;
			else HEAD = p->next;

			if (p->next) p->next->prev = p->prev;
			else TAIL = p->prev;
			counter--;
			return p;
		}
		else
		{
			cout << "\nPicking error! \n";
			return NULL;
		}
	}

	//generowanie talii
	void deck::genDeck()
	{
		card* p;
		for (int i = 2; i < 15; i++)
		{
			p = new card;
			p->value = i;
			p->kolor = card::pik;
			deck::push_front(p);
		}

		for (int i = 2; i < 15; i++)
		{
			p = new card;
			p->value = i;
			p->kolor = card::kier;
			deck::push_front(p);
		}

		for (int i = 2; i < 15; i++)
		{
			p = new card;
			p->value = i;
			p->kolor = card::karo;
			deck::push_front(p);
		}

		for (int i = 2; i < 15; i++)
		{
			p = new card;
			p->value = i;
			p->kolor = card::trefl;
			deck::push_front(p);
		}
	}

	void deck::shuffle()
	{
		srand(time(NULL));
		card* p;
		int n,z;
		for (int i = 0; i < 1000; i++)
		{
			n = rand() % 52 + 1;
			do
			{
				z = rand() % 52 + 1;
			} while (z == n);
			//cout << n << " " << z << endl;
			//p=this->pick(this->index(z));
			//this->pick(this->index(2));
			this->insert(pick(this->index(n)),this->index(z));
		}
	}

	void deck::display()
	{
		card * p;
		int i = 1;
		if (!HEAD) cout << "Lista jest pusta." << endl;
		else
		{
			p = HEAD;
			while (p)
			{
				cout << "Karta nr:" << i << "  ";
				switch (p->value)
				{
				case 14:
					cout << "As ";
					break;
				case 13:
					cout << "Krol ";
					break;
				case 12:
					cout << "Dama ";
					break;
				case 11:
					cout << "Walet ";
					break;
				default:
					cout << p->value << " ";
					break;
				}
				switch (p->kolor)
				{
				case 0:
					cout << "pik" << endl;
					break;
				case 1:
					cout << "kier" << endl;
					break;
				case 2:
					cout << "karo" << endl;
					break;
				case 3:
					cout << "trefl" << endl;
					break;
				}
				p = p->next;
				i++;
			}
			cout << endl;
		}
	}